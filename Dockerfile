FROM node:20-alpine

WORKDIR /app

COPY ./package*.json .
RUN npm install

ARG VERSION='0.0.1'
ENV VERSION=${VERSION}

ENV SECRET=""

COPY ./index.js .

EXPOSE 3000
CMD ["node", "./index.js"]
